import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ReduceLog extends Reducer<Text, IntWritable, Text, IntWritable>{
    private final IntWritable reduceValue = new IntWritable(0);

    @Override
    public void reduce(final Text key, final Iterable<IntWritable> records,
                       final Context context) throws IOException, InterruptedException {

        int sum = 0;
        for (IntWritable record : records) {
            sum += record.get();
        }
        reduceValue.set(sum);
        context.write(key, reduceValue);
    }
}

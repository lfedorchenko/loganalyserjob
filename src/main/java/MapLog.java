import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MapLog extends Mapper<Object, Text, Text, IntWritable>{

    final Set<String> interestingWordsSet = new HashSet<String>();
    //Construct the Text only once and reuse
    private final Text mapKeyText = new Text();
    private static final IntWritable ONE = new IntWritable(1);

    /**
     * Called before processing each input split. In other words,
     * before each file or 64MB of a large file.
     * Load interesting words into a HashSet to check later.
     *
     * @param context - Map Context object
     */
    @Override
    public void setup(final Context context) throws IOException {
        //Load words from the DistributedCache
        File interestingWordsFile = new File("/Users/lfedorchenko/Documents/loganalyser/src/main/java/words.txt");
        BufferedReader reader;
        FileReader fr;
        try {
            fr = new FileReader(interestingWordsFile);
            reader = new BufferedReader(fr);
            String word;
            while ((word = reader.readLine()) != null) {
                interestingWordsSet.add(word);
            }
            fr.close();
            reader.close();
        } catch (IOException ioe) {
            throw new RuntimeException("Could not populate word set", ioe);
        }
    }

    /**
     * INPUT: Block of text
     * OUTPUT: Key: Text Word, Value: 1
     *
     * @param key - Unique Identifier for map (ignored)
     * @param value - MapReduce Text object containing a String
     * @param context - Map Context for reporting and configuration
     */
    @Override
    public void map(final Object key, final Text value, final Context context)
            throws IOException, InterruptedException {
        // Split String into words, naively
        StringTokenizer tokenizer = new StringTokenizer(value.toString());
        while (tokenizer.hasMoreTokens()) {
            if (interestingWordsSet.contains(value)) {
                mapKeyText.set(tokenizer.nextToken());
                context.write(mapKeyText, ONE);
            } else {
                context.getCounter("WordCount", "UNinteresting Words").increment(1L);
            }
        }
    }


}
